var gulp = require('gulp'),
sass = require('gulp-sass'),
browserSync = require('browser-sync'),
autoprefixer = require('gulp-autoprefixer'),
uglify = require('gulp-uglify'),
rename = require('gulp-rename'),
cssnano = require('gulp-cssnano'),
sourcemaps = require('gulp-sourcemaps'),
browserify = require('gulp-browserify');

gulp.task('css', function () {
return gulp.src('src/styles/main.scss')
	.pipe(sourcemaps.init())
	.pipe(sass().on('error', sass.logError))
	.pipe(autoprefixer({
		browsers: ['last 16 versions']
	}))
	.pipe(gulp.dest('public/css'))
	.pipe(sourcemaps.write())
	.pipe(gulp.dest('public/css'))
	.pipe(browserSync.reload({stream: true}));
});

gulp.task('preloader', function () {
return gulp.src('src/styles/preloader.scss')
	.pipe(sourcemaps.init())
	.pipe(sass().on('error', sass.logError))
	.pipe(autoprefixer({
		browsers: ['last 16 versions']
	}))
	.pipe(gulp.dest('public/css'))
	.pipe(sourcemaps.write())
	.pipe(gulp.dest('public/css'))
	.pipe(browserSync.reload({stream: true}));
});

gulp.task('css-min', function () {
return gulp.src('src/styles/main.scss')
	.pipe(sass().on('error', sass.logError))
	.pipe(autoprefixer({
		browsers: ['last 16 versions']
	}))
	.pipe(cssnano({
		zindex: false,
		reduceIdents: false
	}))
	.pipe(gulp.dest('public/css'))
});

gulp.task('preloader-min', function () {
return gulp.src('src/styles/preloader.scss')
	.pipe(sass().on('error', sass.logError))
	.pipe(autoprefixer({
		browsers: ['last 16 versions']
	}))
	.pipe(cssnano({
		zindex: false,
		reduceIdents: false
	}))
	.pipe(gulp.dest('public/css'))
});

gulp.task('js', function () {
gulp.src('src/js/*.js')
	.pipe(sourcemaps.init())
	.pipe(browserify({
		insertGlobals : true,
		debug : true
	}))
	.pipe(gulp.dest('public/js'))
	.pipe(sourcemaps.write())
	.pipe(gulp.dest('public/js'))
	.pipe(browserSync.reload({stream: true, once: true}));
});

gulp.task('js-min', function () {
gulp.src('src/js/*.js')
	.pipe(browserify({
		insertGlobals : true,
		debug : true
	}))
	.pipe(uglify())
	.pipe(gulp.dest('public/js'))
});

gulp.task('pages', function () {
gulp.src(['src/index.html'])
	.pipe(gulp.dest('public'));
});

gulp.task('browser-sync', function () {
	browserSync.init(null, {
		server: {
			baseDir: "public"
		},
		reloadDelay: 800
	});
});

gulp.task('bs-reload', function () {
	browserSync.reload();
});

gulp.task('watch', ['css', 'preloader', 'js', 'pages', 'browser-sync'], function () {
	gulp.watch("src/styles/*/*.scss", ['css']);
	gulp.watch("src/styles/preloader.scss", ['preloader']);
	gulp.watch("src/index.html", ['pages', 'bs-reload']);
	gulp.watch("src/js/*.js", ['js']);
});

gulp.task('build', ['css-min', 'preloader-min', 'js-min', 'pages']);
