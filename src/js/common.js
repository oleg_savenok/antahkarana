import Scrollbar from 'smooth-scrollbar';
import OverscrollPlugin from 'smooth-scrollbar/plugins/overscroll';
import DrawSvg from 'drawsvg';
import { ECHILD } from 'constants';

// Overscroll plugin for scrollbar
Scrollbar.use(OverscrollPlugin);

(function () {

	// Preloader counter animation
	var options = {
		useEasing: true,
		useGrouping: true,
		separator: ',',
		decimal: '.',
	};

	var preloaderCounter = new CountUp('preloaderCounter', 0, 0, 0, 6, options);

	if (!preloaderCounter.error) {
		preloaderCounter.start();
	} else {
		console.error(preloaderCounter.error);
	}

	// Scrollbar init and setting
	const scrollSelector = document.querySelector('#page');

	const scrollOptions = {
		damping: "0.15",
		thumbMinSize: 10,
		// overscrollEffect: false,
		// overscrollEffectColor: '#080920',
		// overscrollDamping: 0.9,
		renderByPixels: true
	};

	const scrollbar = Scrollbar.init(scrollSelector, scrollOptions);

	// Add overscroll plugin for scrollbar
	scrollbar.updatePluginOptions('overscroll', {effect: 'bounce'});

	// ParticlesJS init
	particlesJS.load('particles', './assets/js/particles.json', function() {
		setTimeout(function () {
			window.dispatchEvent(new Event('resize'));
			console.log('Resize!')
		}, 200)
	});

	// Scroll actions
	$('#scrollDown').click(function (e) {
		scrollbar.scrollIntoView(document.querySelector('#team'));
	});

	$('#headerLogo img, .go-to-top').click(function (e) {
		scrollbar.scrollIntoView(document.querySelector('#home'));
	});

	$('.anchor-link').click(function (e) {
		console.log(e.target.hash);
		scrollbar.scrollIntoView(document.querySelector(e.target.hash));
	});

	$('#playVideo').click( (e) => { $('#video').addClass('is-open'); $('#video video').get(0).play(); });

	$('#video').click( (e) => { $('#video').removeClass('is-open'); $('#video video').get(0).pause(); });

	// Navigation action function
	let hamburger = $('#hamburger');
	let page = $('#page');
	let nav = $('#navigation');
	let navItem = $('#navigation a');
	let scrollDown = $('#scrollDown');
	let headerLogo = $('#headerLogo');

	function navigationAction(state) {
		if (state == 'close') {
			hamburger.removeClass('is-open');
			nav.removeClass('is-open');
			page.removeClass('is-unusable');
			scrollDown.removeClass('is-open-nav')

			if ($(window).width() < 767) {
				headerLogo.removeClass('is-open-nav');
			}
		} else
		if (state == 'toggle') {
			hamburger.toggleClass('is-open');
			nav.toggleClass('is-open');
			page.toggleClass('is-unusable');
			scrollDown.toggleClass('is-open-nav');

			if ($(window).width() < 767) {
				headerLogo.toggleClass('is-open-nav');
			}
		}
	}

	// Open/Close on click to hamburger
	hamburger.click( (e) => navigationAction('toggle') );

	// Close on click outside a nav
	$(document).click( (e) => { if (nav.has(e.target).length === 0) {navigationAction('close')} });

	// Close on nav item click
	navItem.click( (e) => navigationAction('close') );

	// Get a reference to the <path1>
	var path1 = document.querySelector('#path-1');
	var path2 = document.querySelector('#path-2');
	var path3 = document.querySelector('#path-3');
	var path4 = document.querySelector('#path-4');
	var path5 = document.querySelector('#path-5');
	var path6 = document.querySelector('#path-6');
	var path7 = document.querySelector('#path-7');
	var path8 = document.querySelector('#path-8');

	// Get length of path1... ~577px in this case
	var path1Length = path1.getTotalLength();
	var path2Length = path2.getTotalLength();
	var path3Length = path3.getTotalLength();
	var path4Length = path4.getTotalLength();
	var path5Length = path5.getTotalLength();
	var path6Length = path6.getTotalLength();
	var path7Length = path7.getTotalLength();
	var path8Length = path8.getTotalLength();

	// Make very long dashes (the length of the path1 itself)
	path1.style.strokeDasharray = path1Length + ' ' + path1Length;
	path2.style.strokeDasharray = path2Length + ' ' + path2Length;
	path3.style.strokeDasharray = path3Length + ' ' + path3Length;
	path4.style.strokeDasharray = path4Length + ' ' + path4Length;
	path5.style.strokeDasharray = path5Length + ' ' + path5Length;
	path6.style.strokeDasharray = path6Length + ' ' + path6Length;
	path7.style.strokeDasharray = path7Length + ' ' + path7Length;
	path8.style.strokeDasharray = path8Length + ' ' + path8Length;


	// Offset the dashes so the it appears hidden entirely
	path1.style.strokeDashoffset = path1Length;
	path2.style.strokeDashoffset = path2Length;
	path3.style.strokeDashoffset = path3Length;
	path4.style.strokeDashoffset = path4Length;
	path5.style.strokeDashoffset = path5Length;
	path6.style.strokeDashoffset = path6Length;
	path7.style.strokeDashoffset = path7Length;
	path8.style.strokeDashoffset = path8Length;

	// Jake Archibald says so
	path1.getBoundingClientRect();
	path2.getBoundingClientRect();
	path3.getBoundingClientRect();
	path4.getBoundingClientRect();
	path5.getBoundingClientRect();
	path6.getBoundingClientRect();
	path7.getBoundingClientRect();
	path8.getBoundingClientRect();

	function isVisible(target) {
		if ( scrollbar.isVisible(document.querySelector(target)) ) {
			return true;
		} else {
			return false;
		}
	}

	// Smooth Scroll Listener
	scrollbar.addListener(function(status) {

		// Particles background parallax
		$('#particles').css('transform', 'translateY(-' + status.offset.y / 30 + 'px)');

		// Header hide
		if (status.offset.y > 100) {
			$('#header').addClass('is-hidden');
		} else {
			$('#header').removeClass('is-hidden');
		}

		// Footer button hide
		if (status.offset.y > 100) {
			$('#scrollDown, #playVideo').addClass('is-hidden');
		} else {
			$('#scrollDown, #playVideo').removeClass('is-hidden');
		}

		// Footer show
		if (status.offset.y > $('.scroll-content').height() - $(window).height() - 70) {
			$('#footerMenu').addClass('is-visible');
		} else {
			$('#footerMenu').removeClass('is-visible');
		}

		// Home screen logo hide
		$('#homeLogo').css({
			'opacity': 1 - (status.offset.y / $(window).height() * 2),
			'marginTop': status.offset.y / 1.5,
		});

		// Unfocused input on scroll
		if ($('input').is(':focus')) {
			$('input').trigger('blur');
		}

		// SVG Drawing on scroll
		var scrollPercentage1 = status.offset.y / ( ( $('#team').outerHeight() * 1.6) );
		var scrollPercentage2 = (status.offset.y - $('#team').outerHeight() - $('#projects').outerHeight() ) / ( ( $('#webinars').outerHeight() * 1.3 ) );

		// Length to offset the dashes
		var drawLength1 = path1Length * scrollPercentage1;
		var drawLength2 = path2Length * scrollPercentage1;
		var drawLength3 = path3Length * scrollPercentage1;
		var drawLength4 = path4Length * scrollPercentage1;
		var drawLength5 = path5Length * scrollPercentage2;
		var drawLength6 = path6Length * scrollPercentage2;
		var drawLength7 = path7Length * scrollPercentage2;
		var drawLength8 = path8Length * scrollPercentage2;

		// Draw in reverse
		path1.style.strokeDashoffset = path1Length - drawLength1;
		path2.style.strokeDashoffset = path2Length - drawLength2;
		path3.style.strokeDashoffset = path3Length - drawLength3;
		path4.style.strokeDashoffset = path4Length - drawLength4;
		path5.style.strokeDashoffset = path5Length - drawLength5;
		path6.style.strokeDashoffset = path6Length - drawLength6;
		path7.style.strokeDashoffset = path7Length - drawLength7;
		path8.style.strokeDashoffset = path8Length - drawLength8;

		// When complete, remove the dash array, otherwise shape isn't quite sharp
		// Accounts for fuzzy math
		if (scrollPercentage1 >= 0.99) {
			path1.style.strokeDasharray = "none";
			path2.style.strokeDasharray = "none";
			path3.style.strokeDasharray = "none";
			path4.style.strokeDasharray = "none";
		} else {
			path1.style.strokeDasharray = path1Length + ' ' + path1Length;
			path2.style.strokeDasharray = path2Length + ' ' + path2Length;
			path3.style.strokeDasharray = path3Length + ' ' + path3Length;
			path4.style.strokeDasharray = path4Length + ' ' + path4Length;
		}

		if (scrollPercentage2 >= 0.99) {
			path5.style.strokeDasharray = "none";
			path6.style.strokeDasharray = "none";
			path7.style.strokeDasharray = "none";
			path8.style.strokeDasharray = "none";
		} else {
			path5.style.strokeDasharray = path5Length + ' ' + path5Length;
			path6.style.strokeDasharray = path6Length + ' ' + path6Length;
			path7.style.strokeDasharray = path7Length + ' ' + path7Length;
			path8.style.strokeDasharray = path8Length + ' ' + path8Length;
		}

		// Team members item animation
		if ( isVisible('.team__members__item') ) {
			$('.team__members__item').addClass('is-visible');
		} else {
			$('.team__members__item').removeClass('is-visible');
		}

	});

	// Slider init and setting
	$('#webinarsSlider').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		infinite: true,
		//autoplay: true,
		autoplaySpeed: 5000,
		speed: 500,
		draggable: true,
		centerPadding: '5px',
		prevArrow: $('#webinarsSliderPrev'),
		nextArrow: $('#webinarsSliderNext'),
	});

	// Send contact form
	$('#sendForm').click(function (e) {
		$('#contactForm').submit();
	});

	preloaderCounter.update(100);
	$('#preloader').addClass('is-hidden');

	setTimeout(function () {
		window.dispatchEvent(new Event('resize'));
		console.log('Resize!')
	}, 500)

}());